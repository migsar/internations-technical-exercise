import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Users from '@/components/Users'
import User from '@/components/User'
import Groups from '@/components/Groups'
import Group from '@/components/Group'
import NotFound from '@/components/NotFound'

Vue.use(Router)

export const routes = [
  { path: '/', component: Home },
  { path: '/users', component: Users },
  { path: '/user', component: User },
  { path: '/user/:id', component: User },
  { path: '/groups', component: Groups },
  { path: '/group', component: Group },
  { path: '/group/:id', component: Group },
  { path: '*', component: NotFound }
]

export default new Router({
  mode: 'history',
  routes
})
