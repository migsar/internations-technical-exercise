/**
 * API service
 * All the methods here could be binded to backend endpoints
 * By using Objects as models we could group some user or group
 * operations together.
 */
class API {
  constructor () {
    // This should be retrieved from a server
    // It is populated to make it easy to see the app working
    this.users = [
      {
        id: 'sor_juana_ines_de_la_cruz',
        name: 'Sor Juana Ines de la Cruz',
        email: 'sor_juana_ines_de_la_cruz@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'manuel_gutierrez_najera',
        name: 'Manuel Gutierrez Najera',
        email: 'manuel_gutierrez_najera@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'amado_nervo',
        name: 'Amado Nervo',
        email: 'amado_nervo@example.com',
        phone: '+52 1 55 3414 4567',
        birthdate: '1919-05-24T05:00:00.000Z'
      },
      {
        id: 'carlos_pellicer',
        name: 'Carlos Pellicer',
        email: 'carlos_pellicer@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'octavio_paz',
        name: 'Octavio Paz',
        email: 'octavio_paz@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'luis_vaz_de_camoes',
        name: 'Luís Vaz de Camões',
        email: 'luis_vaz_de_camoes@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'eca_de_queiros',
        name: 'Eça de Queirós',
        email: 'eca_de_queiros@example.com',
        phone: '+351 3285 53434',
        birthdate: '1845-11-25T06:00:00.000Z'
      },
      {
        id: 'fernando_pessoa',
        name: 'Fernando Pessoa',
        email: 'fernando_pessoa@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'jose_saramago',
        name: 'José Saramago',
        email: 'jose_saramago@example.com',
        phone: '',
        birthdate: ''
      },
      {
        id: 'eugen_berthold_friedrich_brecht',
        name: 'Eugen Berthold Friedrich Brecht',
        email: 'eugen_berthold_friedrich_brecht@example.com',
        phone: '',
        birthdate: '1898-02-10T06:00:00.000Z'
      }
    ]
    this.groups = [
      {
        id: 'mexico',
        name: 'Mexico',
        users: [
          'sor_juana_ines_de_la_cruz',
          'manuel_gutierrez_najera',
          'amado_nervo',
          'carlos_pellicer',
          'octavio_paz'
        ]
      },
      {
        id: 'portugal',
        name: 'Portugal',
        users: [
          'luis_vaz_de_camoes',
          'eca_de_queiros',
          'fernando_pessoa',
          'jose_saramago'
        ]
      },
      {
        id: 'germany',
        name: 'Germany',
        users: [
          'eugen_berthold_friedrich_brecht',
          'sor_juana_ines_de_la_cruz'
        ]
      }
    ]
  }

  /**
   * Get users
   * @return {Object[]} - Users
   */
  getUsers () {
    return this.users
  }

  /**
   * Get user by id
   * @param {string} id - User id
   * @return {Object} - User
   */
  getUser (id) {
    return this.users.find(usr => usr.id === id)
  }

  /**
   * Get user groups by user id
   * @param {string} id - User id
   * @return {Object} - Groups
   */
  getUserGroups (id) {
    return this.groups.filter(grp => grp.users.includes(id))
  }

  /**
   * Delete a user
   * @param {string} id - User id
   */
  deleteUser (id) {
    this.users = this.users.filter(usr => usr.id !== id)
    // Not very efficient, I know
    this.groups.forEach(grp => this.removeUserFromGroup(id, grp.id))
  }

  /**
   * Add user to group
   * @param {string} id - User id
   * @param {string} grpId - Group id
   */
  addUserToGroup (id, grpId) {
    this.getGroup(grpId).users.push(id)
  }

  /**
   * Remove user from group
   * @param {string} id - User id
   * @param {string} grpId - Group id
   * @return {Object} - Groups
   */
  removeUserFromGroup (id, grpId) {
    // This could be simplified but is more readable like this
    const grp = this.getGroup(grpId)
    const isUserInGroup = grp && grp.users && grp.users.includes(id)

    if (!grp || !isUserInGroup) {
      return
    }

    const users = grp.users
    const userIndex = users.indexOf(id)
    grp.users = [...users.slice(0, userIndex), ...users.slice(userIndex + 1)]
  }

  /**
   * Get groups
   * @return {Object[]} - Groups
   */
  getGroups () {
    return this.groups
  }

  /**
   * Get group by id
   * @param {string} id - Group id
   * @return {Object} - Group
   */
  getGroup (id) {
    return this.groups.find(grp => grp.id === id)
  }

  /**
   * Get users in group
   * @param {string} id - Group id
   * @return {Object} - Group
   */
  getGroupUsers (id) {
    const userIds = this.groups.find(grp => grp.id === id).users
    return this.users.filter(usr => userIds.includes(usr.id))
  }

  /**
   * Save user
   * @param {Object} user - The user to save
   */
  saveUser (user) {
    const { group: grpId } = user
    delete user.group
    this.users.push(user)
    this.groups.find(grp => grp.id === grpId).users.push(user.id)
  }

  /**
   * Save group
   * For a real api most of this method would return promises
   * for being aware of success or fail
   * @param {Object} group - The group to save
   */
  saveGroup (group) {
    this.groups.push(group)
  }

  /**
   * Delete a group
   * @param {string} id - Group id
   */
  deleteGroup (id) {
    this.groups = this.groups.filter(grp => grp.id !== id)
  }
}

export default new API()
