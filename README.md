# internations

> Internations Technical Exercise

## App Routes

```
  /          -> Project Home Page
  /users     -> List users
  /user      -> New user
  /user/:id  -> User details
  /groups    -> List groups
  /group     -> New Groups
  /group/:id -> Group details
```

## User Management System

```
// User model
{
  name: string,
  email: string,
  phone: string,
  birthdate: string // Date.toISOString()
}

// Group model
{
  name: string,
  users: User[]
}
```

### Expected API

The app was conceived with a service for retrieving the information, it works with JSON now and handles part of the state (the persistance supposed to be on the server side).
The expected endpoints would be the usual for a CRUD system, being the first three of each resource the really essential.

```
 GET    /users
 GET    /user
 POST   /user
 PUT    /user
 DELETE /user
 GET    /groups
 GET    /group
 POST   /group
 PUT    /group
 DELETE /group
```

A GraphQL endpoint would be nice because it could simplify front-end logic, with the proposed models a query to groups is needed to know if the user is part of it.

### The process

I decided to go on the side of using a new framework instead of a well-know one, I choose Vue, because I've heard it is really cool and after using it for this project I can say I really like it.
This decision was not the best in term of fullfiling the full-specification, nevertheless the basic features are there and I am happy with the decision even if it mean that the project extended a bit
more over the suggested time, not really working on conceptual things but on how to get things done with Vue.

I decide to explore what Vue had for state management, I think is very powerful but I need a bit more expertise, probably it would have been a good idea to use Mobx or Redux for state management.
Some components are more complex than they should, meaning that I should have taken some parts of the code into separate components.

## Project setup
### Compiles and hot-reloads for development
```
yarn install
yarn dev
```

### Compiles and minifies for production
```
yarn install
yarn build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
